pt-BR:

O emulador de computadores fMSX foi criado por Marat Fayzullin. Emula os computadores domésticos MSX, MSX2 e MSX2+ de 8 bits.

Contém: o arquivo ou pacote de instalação .deb e um arquivo de texto em idioma "pt-BR" explicando como utilizá-lo.

Baixe ou descarregue ou transfira os arquivos "fmsx_4.9-1_amd64.deb", "fmsx_4.9-1_amd64.deb.md5.sum", "fmsx_4.9-1_amd64.deb.sha256.sum" e o arquivo de texto ".txt" que estão no arquivo compactado ".zip"

Todos os créditos e direitos estão incluídos nos arquivos, em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Der Computeremulator fMSX wurde von Marat Fayzullin entwickelt. Emuliert 8-Bit-Heimcomputer MSX, MSX2 und MSX2+.

Es enthält: die .deb-Installationsdatei oder das Paket und eine Textdatei in "pt-BR"-Sprache, die erklärt, wie man es benutzt.

Laden Sie die Dateien „fmsx_4.9-1_amd64.deb“, „fmsx_4.9-1_amd64.deb.md5.sum", „fmsx_4.9-1_amd64.deb.sha256.sum“ und die Textdatei „.txt“ herunter in der komprimierten „.zip“-Datei.

Alle Credits und Rechte sind in den Dateien enthalten, in Bezug auf die freiwillige Arbeit jeder Person, die teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

en:

The fMSX computer emulator was created by Marat Fayzullin. Emulates 8-bit MSX, MSX2 and MSX2+ home computers.

Contains: the .deb installation file or package and a text file in "pt-BR" language explaining how to use it.

Download the files "fmsx_4.9-1_amd64.deb", "fmsx_4.9-1_amd64.deb.md5.sum", "fmsx_4.9-1_amd64.deb.sha256.sum" and the text file ".txt" and the text file " .txt" that are in the compressed file ".zip".

All credits and rights are included in the files, in respect of the volunteer work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe

- - - - -

es:

El emulador de computadora fMSX fue creado por Marat Fayzullin. Emula ordenadores domésticos MSX, MSX2 y MSX2+ de 8 bits.

Contiene: el archivo o paquete de instalación .deb y un archivo de texto en idioma "pt-BR" que explica cómo usarlo.

Descargue los archivos "fmsx_4.9-1_amd64.deb", "fmsx_4.9-1_amd64.deb.md5.sum", "fmsx_4.9-1_amd64.deb.sha256.sum" y el archivo de texto ".txt" que se encuentran en el archivo comprimido ".zip".

Todos los créditos y derechos están incluidos en los archivos, en relación con el trabajo voluntario de cada persona que participó y colaboró ​​para que estos archivos pudieran estar disponibles en este sitio web.

marcelocripe

- - - - -

fr :

L'émulateur informatique fMSX a été créé par Marat Fayzullin. Émule les ordinateurs domestiques MSX, MSX2 et MSX2+ 8 bits.

Contient : le fichier ou package d'installation .deb et un fichier texte en "pt-BR" expliquant comment l'utiliser.

Téléchargez les fichiers "fmsx_4.9-1_amd64.deb", "fmsx_4.9-1_amd64.deb.md5.sum", "fmsx_4.9-1_amd64.deb.sha256.sum" et le fichier texte ".txt" qui se trouvent dans le fichier compressé ".zip".

Tous les crédits et droits sont inclus dans les fichiers, dans le respect du travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site.

marcelocripe

- - - - -

it:

L'emulatore di computer fMSX è stato creato da Marat Fayzullin. Emula i computer domestici MSX, MSX2 e MSX2+ a 8 bit.

Contiene: il file o pacchetto di installazione .deb e un file di testo in lingua "pt-BR" che spiega come utilizzarlo.

Scarica i file "fmsx_4.9-1_amd64.deb", "fmsx_4.9-1_amd64.deb.md5.sum", "fmsx_4.9-1_amd64.deb.sha256.sum" e il file di testo ".txt" che si trovano nel file compresso ".zip".

Tutti i crediti e i diritti sono inclusi nei file, nel rispetto del lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.

marcelocripe
